==================
Image hosting task
==================

Requires Python 3.6 and higher, exiftool (perl-Image-ExifTool)

Install dependencies:
.. code-block:: bash
    pip install -r requirements.txt

