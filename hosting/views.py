from django.urls import reverse
from django.views.generic import FormView, DetailView, CreateView, ListView, DeleteView

from hosting.forms import ImageFileUploadForm
from hosting.models import HostedImage


class HomePageView(FormView):

    template_name = "index.html"
    form_class = ImageFileUploadForm


class UploadView(CreateView):

    form_class = ImageFileUploadForm
    template_name = "index.html"


class ImageView(DetailView):

    model = HostedImage
    context_object_name = "image"
    template_name = "image_view.html"


class ImageListView(ListView):

    queryset = HostedImage.objects.all()
    paginate_by = 10
    context_object_name = "images"
    template_name = "image_list.html"


class RemoveImageView(DeleteView):

    model = HostedImage

    def get_success_url(self):
        return "{}?remove=success".format(reverse("hosting:image_list"))

