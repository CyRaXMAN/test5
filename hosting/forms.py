from django import forms
from django.core.exceptions import ValidationError
from django.utils.translation import gettext as _

from hosting.models import HostedImage, hash_file_object


class ImageFileUploadForm(forms.ModelForm):

    def clean(self):
        super().clean()
        image_file = self.cleaned_data.get('image_file')
        if not image_file:
            raise ValidationError(
                {"image_file": _("image file is corrupted or too big")}, code='invalid'
            )

        image_file.file.seek(0)
        sha256_hash = hash_file_object(image_file)
        if HostedImage.objects.filter(sha256_checksum=sha256_hash).exists():
            raise ValidationError(
                {"image_file": _("same image file already exists")}, code='invalid'
            )

        return self.cleaned_data

    class Meta:
        model = HostedImage
        fields = ('name', 'image_file')
