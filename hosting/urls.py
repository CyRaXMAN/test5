from django.urls import path

from hosting.views import HomePageView, ImageView, UploadView, ImageListView, RemoveImageView

app_name = 'hosting'

urlpatterns = [
    path('', HomePageView.as_view(), name="home_page"),
    path('upload/', UploadView.as_view(), name="upload_handler"),
    path('view/<int:pk>', ImageView.as_view(), name="image_view"),
    path('list/', ImageListView.as_view(), name="image_list"),
    path('remove/<int:pk>', RemoveImageView.as_view(), name="image_remove")
]