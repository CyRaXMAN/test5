import hashlib

from django.db import models
from django.utils import timezone
from django.utils.translation import gettext as _
from exiffield.fields import ExifField
from model_utils import FieldTracker


def hash_file_object(file_object):
    """
    Hash uploaded file

    :param file_object: Django input file object
    :type file_object: FieldFile|ImageFieldFile
    :return: hash digest
    :rtype: str
    """
    hash_object = hashlib.sha256()
    for chunk in iter(lambda: file_object.read(4096), b""):
        hash_object.update(chunk)

    return hash_object.hexdigest()


class HostedImage(models.Model):

    name = models.CharField(_("image name"), max_length=1000)
    image_file = models.ImageField(_("image file"), upload_to="uploads/")
    exif = ExifField(source='image_file')
    upload_date = models.DateTimeField(_("upload date"), default=timezone.now)
    sha256_checksum = models.CharField(_("checksum"), max_length=64, db_index=True, unique=True)
    image_file_tracker = FieldTracker(fields=['image_file'])

    def save(self, *args, **kwargs):
        if self.image_file_tracker.changed():
            self.image_file.file.seek(0)
            self.sha256_checksum = hash_file_object(self.image_file.file)

        super().save(*args, **kwargs)

    def get_absolute_url(self):
        from django.urls import reverse
        return reverse('hosting:image_view', args=(self.id,))

    class Meta:
        verbose_name = _("image file")
        verbose_name_plural = _("image files")

    def __str__(self):
        return self.name
